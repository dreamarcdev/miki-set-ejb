package com.miki.set.ejb;

import java.util.List;

import javax.ejb.Remote;

import com.miki.set.jpa.object.StockResult;

@Remote
public interface StockResultRepositoryRemote {
	public List<StockResult> findAll();
}
