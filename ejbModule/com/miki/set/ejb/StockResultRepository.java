package com.miki.set.ejb;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.miki.set.jpa.object.StockResult;

/**
 * Session Bean implementation class SetResultFacade
 */
@Stateless
@LocalBean
@WebService(serviceName="StockResultService")
public class StockResultRepository implements StockResultRepositoryRemote, StockResultRepositoryLocal {

	 @PersistenceContext(unitName = "MikiMgmtPU")
	 private EntityManager em;
	 
    /**
     * Default constructor. 
     */
    public StockResultRepository() {
        
    }

	@SuppressWarnings("unchecked")
	@Override
	@WebMethod(operationName="getAll")
	public List<StockResult> findAll() {
		List<StockResult> result = new ArrayList<StockResult>();
		result = em.createQuery("Select t from " + StockResult.class.getSimpleName() + " t").getResultList();
		return result;
	}

}
